package main

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	proto "gitlab.com/forpelevin/go-calendar/integration_tests/internal/protobuf/calendar_service"
	"log"
	"os"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
)

// Creates the Calendar Service client and returns it with corresponding gRPC connection.
// Don't forget to close the connection by yourself.
func createCalendarServiceClient() (proto.CalendarServiceClient, *grpc.ClientConn) {
	// Connect to the gRPC server via TCP.
	address := fmt.Sprintf(
		"%s:%s",
		os.Getenv("CALENDAR_GRPC_SERVER_HOST"),
		os.Getenv("CALENDAR_GRPC_SERVER_PORT"),
	)
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	return proto.NewCalendarServiceClient(conn), conn
}

func TestMain(m *testing.M) {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Read the env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	code := m.Run()
	os.Exit(code)
}

func TestCalendarService(t *testing.T) {
	client, conn := createCalendarServiceClient()
	defer conn.Close()

	var createdEvent *proto.Event
	// Test create.
	t.Run("test the create method", func(t *testing.T) {
		assert := assert.New(t)

		res, err := client.CreateEvent(context.Background(), &proto.CreateEventRequest{
			Event: &proto.Event{
				Title:     "test",
				StartDate: &timestamp.Timestamp{Seconds: time.Now().Unix()},
				EndDate:   &timestamp.Timestamp{Seconds: time.Now().AddDate(0, 1, 0).Unix()},
			}},
		)
		assert.NoError(err)
		assert.NotNil(res.Event)
		assert.NotNil(res.Event.Id)
		assert.Equal("test", res.Event.Title)
		createdEvent = res.Event
	})

	// Test update.
	t.Run("test the update method", func(t *testing.T) {
		assert := assert.New(t)

		newTitle := "new title"
		newEndDate := &timestamp.Timestamp{Seconds: time.Now().AddDate(0, 2, 0).Unix()}
		createdEvent.Title = newTitle
		createdEvent.EndDate = newEndDate
		res, err := client.UpdateEvent(context.Background(), &proto.UpdateEventRequest{Event: createdEvent})
		assert.NoError(err)
		assert.NotNil(res.Event)
		assert.Equal(newTitle, res.Event.Title)
		assert.Equal(newEndDate.Seconds, res.Event.EndDate.Seconds)
	})

	// Test delete.
	t.Run("test the delete method", func(t *testing.T) {
		assert := assert.New(t)

		res, err := client.DeleteEvent(context.Background(), &proto.DeleteEventRequest{EventId: createdEvent.Id})
		assert.NoError(err)
		assert.Equal(createdEvent.Id, res.EventId)
	})

	// Test getting of expired events.
	t.Run("test the method of getting expired events", func(t *testing.T) {
		assert := assert.New(t)

		// Create an event with start date from the past.
		ctx := context.Background()
		res, err := client.CreateEvent(ctx, &proto.CreateEventRequest{
			Event: &proto.Event{
				Title:     "test",
				StartDate: &timestamp.Timestamp{Seconds: time.Now().AddDate(0, -1, 0).Unix()},
				EndDate:   &timestamp.Timestamp{Seconds: time.Now().Unix()},
			}},
		)
		assert.NoError(err)
		assert.NotNil(res.Event)
		assert.NotNil(res.Event.Id)

		event := res.Event

		// Make sure that we can fetch the event from the service.
		expiredEventsRes, err := client.GetExpiredEvents(ctx, &proto.GetExpiredEventsRequest{})
		assert.NoError(err)
		assert.NotEmpty(expiredEventsRes.Events)
		eventExistsInResponse := false
		for _, e := range expiredEventsRes.Events {
			if event.Id == e.Id {
				eventExistsInResponse = true
			}
		}
		assert.True(eventExistsInResponse)
	})
}
