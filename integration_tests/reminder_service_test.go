package main

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	proto "gitlab.com/forpelevin/go-calendar/integration_tests/internal/protobuf/calendar_service"
	_ "github.com/jackc/pgx/stdlib"
	"log"
	"os"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/stretchr/testify/assert"
)

const (
	DbMAxOpenConns                 = 5
	DbMaxIdleConnections           = 100
	DbConnectionMaxLifetimeSeconds = 300 * time.Second
)

func createDb() *sqlx.DB {
	dsn := fmt.Sprintf(
		"user=%s dbname=%s password=%s sslmode=disable host=%s port=%s",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
	)
	db, err := sqlx.Connect("pgx", dsn)
	if err != nil {
		log.Fatal(err)
	}
	db.SetMaxOpenConns(DbMAxOpenConns)
	db.SetMaxIdleConns(DbMaxIdleConnections)
	db.SetConnMaxLifetime(DbConnectionMaxLifetimeSeconds)

	return db
}


func TestNotifyingAboutExpiredEvents(t *testing.T) {
	client, conn := createCalendarServiceClient()
	defer conn.Close()

	t.Run("if there is an event that has expired, a user should be notified", func(t *testing.T) {
		assert := assert.New(t)
		ctx := context.Background()

		// The event already happened
		res, err := client.CreateEvent(ctx, &proto.CreateEventRequest{
			Event: &proto.Event{
				Title:     "test",
				StartDate: &timestamp.Timestamp{Seconds: time.Now().AddDate(0, -1, 0).Unix()},
				EndDate:   &timestamp.Timestamp{Seconds: time.Now().Unix()},
			}},
		)
		assert.NoError(err)
		assert.NotNil(res.Event)
		assert.NotNil(res.Event.Id)
		assert.Equal("test", res.Event.Title)
		event := res.Event

		// To wait until the events will be asynchronously handled.
		time.Sleep(10 * time.Second)

		// Check that the event is notified
		db := createDb()
		row := db.QueryRowxContext(
			ctx,
			"select notified_at from event where id = $1",
			event.Id,
		)
		var notifiedAt string
		err = row.Scan(&notifiedAt)
		assert.NoError(err)
		fmt.Printf("Notified_at is = %+v\n", notifiedAt)
		assert.NotEmpty(notifiedAt)
	})
}
