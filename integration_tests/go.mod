module gitlab.com/forpelevin/go-calendar/integration_tests

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.24.0
)
