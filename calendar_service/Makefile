-include .env
export

.PHONY: test
test: ## Run all the tests
	go test -v -race -timeout=30s ./...

.PHONY: fmt
fmt: ## Run goimports on all go files
	go fmt ./...
	gofmt -w -s internal

.PHONY: lint
lint: ## Run all the linters
	golangci-lint run --enable-all --disable="gochecknoglobals,gochecknoinits,scopelint"

.PHONY: build
build: ## Build a version
	cp -n .env.dist .env || \
	go build -v -a -mod=vendor -installsuffix cgo -o ./main ./cmd/main.go
	chmod +x ./main

.PHONY: migrate
migrate: ## Migrate the DB schema
	bin/migrate -source file:db/migrations -database postgres://${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable\&user=${DB_USERNAME}\&password=${DB_PASSWORD} up

.PHONY: migrate_fresh
migrate_fresh: ## Refreshes the whole DB schema
	bin/migrate -source file:db/migrations -database postgres://${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable\&user=${DB_USERNAME}\&password=${DB_PASSWORD} down -all
	bin/migrate -source file:db/migrations -database postgres://${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable\&user=${DB_USERNAME}\&password=${DB_PASSWORD} up

# Absolutely awesome: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := build