module gitlab.com/forpelevin/go-calendar/calendar_service

go 1.12

require (
	github.com/caarlos0/env/v6 v6.0.0
	github.com/golang-migrate/migrate/v4 v4.6.1
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jackc/pgx v3.2.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.2.1
	github.com/stretchr/testify v1.3.0
	google.golang.org/grpc v1.23.0
	gopkg.in/yaml.v2 v2.2.2
)
