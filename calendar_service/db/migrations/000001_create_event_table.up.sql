CREATE TABLE event (
    id serial primary key,
    title varchar (255),
    start_date timestamp,
    end_date timestamp,
    notified_at timestamp
);