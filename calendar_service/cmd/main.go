package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/forpelevin/go-calendar/calendar_service/cmd/server"
)

func main() {
	// Read the env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	server.Run()
}
