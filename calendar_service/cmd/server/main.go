package server

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	proto "gitlab.com/forpelevin/go-calendar/calendar_service/internal/protobuf/calendar_service"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/database"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/repository"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/server"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Run starts the gRPC server up.
func Run() {
	c, err := server.ReadServerConfig()
	if err != nil {
		log.Fatal(err)
	}

	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Initialize your gRPC server's interceptor.
	s := createGRPCServer(c)
	defer s.Stop()

	// After all your registrations, make sure all of the Prometheus metrics are initialized.
	grpc_prometheus.EnableHandlingTimeHistogram()
	grpc_prometheus.Register(s)
	// Register Prometheus metrics handler.
	http.Handle("/metrics", promhttp.Handler())

	// Register reflection service on gRPC server.
	reflection.Register(s)

	// Start the gRPC server.
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", c.GRPC.Host, c.GRPC.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	defer lis.Close()

	go func() {
		fmt.Printf("Listening on: %s\n", c.GRPC.Port)
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	// Start the HTTP server for Prometheus metrics pulling.
	promLisAddr := fmt.Sprintf("%s:%s", c.Prometheus.ListenHost, c.Prometheus.ListenPort)
	go func() {
		fmt.Printf("Listening on: %s for Prometheus\n", promLisAddr)
		log.Fatal(http.ListenAndServe(promLisAddr, nil))
	}()

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	fmt.Println("Stopping the server")
	fmt.Println("Bye!")
}

func createGRPCServer(c *server.Config) *grpc.Server {
	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			grpc_prometheus.UnaryServerInterceptor,
		),
	)

	// Register our gRPC service.
	db, err := database.GetSqlxConnectionByConfig(c)
	if err != nil {
		log.Fatal(err)
	}
	proto.RegisterCalendarServiceServer(s, service.NewCalendarService(repository.NewDBEventRepositoryWithOptions(db)))

	return s
}
