package repository

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/testutil"
)

func TestDBEventRepository(t *testing.T) {
	db, err := testutil.PrepareDBForTest()
	if err != nil {
		t.Fatal(err)
	}

	repo := &DBEventRepository{
		db: db,
	}

	globalEvent := &Event{}

	// Test insert
	t.Run("test a new event insertion", func(t *testing.T) {
		assert := assert.New(t)
		startDate := time.Now().AddDate(0, 0, 1).UTC()
		endDate := time.Now().AddDate(0, 1, 0).UTC()
		event := &Event{
			Title:     "test",
			StartDate: &startDate,
			EndDate:   &endDate,
		}
		event, err = repo.Create(context.Background(), event)
		assert.NoError(err)
		assert.NotEqual(0, event.Id)
		globalEvent = event
	})

	// Test Get
	t.Run("test an event getting", func(t *testing.T) {
		assert := assert.New(t)
		if err != nil {
			t.Fatal(err)
		}
		event, err := repo.GetByID(context.Background(), globalEvent.Id)
		assert.NoError(err)
		assert.Equal(globalEvent, event)
	})

	// Test getting of an expired event
	t.Run("test an expired event getting", func(t *testing.T) {
		// Given we have 2 events and one of them is expired.
		assert := assert.New(t)
		title := "test expired"
		startDate := time.Now().AddDate(0, -1, 0).UTC()
		endDate := time.Now().AddDate(0, 1, 0).UTC()
		event := &Event{
			Title:     title,
			StartDate: &startDate,
			EndDate:   &endDate,
		}
		event, err = repo.Create(context.Background(), event)
		assert.NoError(err)
		assert.Equal(event.Title, title)

		events, err := repo.GetExpiredEvents(context.Background())
		assert.NoError(err)
		if len(events) < 1 {
			t.Error("expected that there is at least 1 expired event")
		}

		firstEvent := events[0]
		assert.Equal(firstEvent.Title, event.Title)
	})

	// Test update
	t.Run("test an event update", func(t *testing.T) {
		assert := assert.New(t)
		newTitle := "new title"
		globalEvent.Title = newTitle
		err = repo.Edit(context.Background(), globalEvent)
		assert.NoError(err)

		event, err := repo.GetByID(context.Background(), globalEvent.Id)
		assert.NoError(err)
		assert.Equal(globalEvent, event)
	})

	// Test delete
	t.Run("test an event deletion", func(t *testing.T) {
		assert := assert.New(t)
		err := repo.Delete(context.Background(), globalEvent.Id)
		assert.NoError(err)

		event, err := repo.GetByID(context.Background(), globalEvent.Id)
		assert.Nil(event)
		assert.Equal(ErrNotFound, err)
	})
}
