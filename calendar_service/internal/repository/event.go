package repository

import (
	"context"
	"database/sql"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

var (
	ErrNotFound = errors.New("there is no event with such ID")
)

// Event implements a calendar event.
type Event struct {
	Id         int64
	Title      string
	StartDate  *time.Time `db:"start_date"`
	EndDate    *time.Time `db:"end_date"`
	NotifiedAt *time.Time `db:"notified_at"`
}

// EventRepository is an abstraction for an event storage.
type EventRepository interface {
	// GetByID searches an event in storage by the ID and returns it if it was found.
	GetByID(ctx context.Context, id int64) (*Event, error)

	// GetExpiredEvents returns the events that have come already.
	GetExpiredEvents(ctx context.Context) ([]*Event, error)

	// Create creates a new event, stores it in a storage and returns the created entity object with filled ID.
	Create(ctx context.Context, e *Event) (*Event, error)

	// Edit replaces an event with the ID in a storage by the e event. In case of failure, it returns an error.
	Edit(ctx context.Context, e *Event) error

	// Delete deletes an event by the ID. In case of failure, it returns an error.
	Delete(ctx context.Context, id int64) error
}

// MemoryEventRepository is an event storage that keeps all events in memory.
type MemoryEventRepository struct {
	events       map[int64]*Event
	eventCounter int64
	mutex        sync.Mutex
}

// GetByID searches an event in memory by the ID and returns it if it was found.
func (mer *MemoryEventRepository) GetByID(ctx context.Context, id int64) (*Event, error) {
	val, ok := mer.events[id]
	if !ok {
		return nil, nil
	}

	return val, nil
}

// GetExpiredEvents returns the events that have come already.
func (mer *MemoryEventRepository) GetExpiredEvents(ctx context.Context) ([]*Event, error) {
	currentTime := time.Now()
	expiredEvents := make([]*Event, 0)
	for _, e := range mer.events {
		if e.StartDate == nil || e.NotifiedAt != nil {
			continue
		}

		if e.StartDate.Before(currentTime) {
			expiredEvents = append(expiredEvents, e)
		}
	}

	return expiredEvents, nil
}

// Create creates a new event, stores it in memory and returns the created entity object with filled ID.
func (mer *MemoryEventRepository) Create(ctx context.Context, e *Event) (*Event, error) {
	mer.mutex.Lock()
	mer.eventCounter++
	e.Id = mer.eventCounter
	mer.events[mer.eventCounter] = e
	mer.mutex.Unlock()

	return e, nil
}

// Edit replaces an event with the ID in a storage by the e event.
func (mer *MemoryEventRepository) Edit(ctx context.Context, e *Event) error {
	mer.mutex.Lock()
	defer mer.mutex.Unlock()

	_, ok := mer.events[e.Id]
	if !ok {
		return ErrNotFound
	}

	mer.events[e.Id] = e

	return nil
}

// Delete deletes an event by the ID from memory. In case of failure, it returns an error.
func (mer *MemoryEventRepository) Delete(ctx context.Context, id int64) error {
	mer.mutex.Lock()
	defer mer.mutex.Unlock()

	_, ok := mer.events[id]
	if !ok {
		return errors.Errorf("nothing to delete. There is no event with the ID: %d", id)
	}

	delete(mer.events, id)

	return nil
}

// NewMemoryEventRepository creates a new memory repository instance.
func NewMemoryEventRepository() *MemoryEventRepository {
	return &MemoryEventRepository{make(map[int64]*Event, 1024), 0, sync.Mutex{}}
}

// NewMemoryEventRepositoryWithEvents creates a new memory repository instance with provided events.
func NewMemoryEventRepositoryWithEvents(events map[int64]*Event) *MemoryEventRepository {
	return &MemoryEventRepository{events, 0, sync.Mutex{}}
}

// DBEventRepository is an event repository that wraps the PostgreSQL.
type DBEventRepository struct {
	db *sqlx.DB
}

// GetByID searches an event in DB by the ID and returns it if it was found.
func (repo *DBEventRepository) GetByID(ctx context.Context, id int64) (*Event, error) {
	var e Event
	row := repo.db.QueryRowxContext(
		ctx,
		"select id, title, start_date, end_date from event where id = $1",
		id,
	)
	err := row.StructScan(&e)
	if err != nil && err == sql.ErrNoRows {
		return nil, ErrNotFound
	}

	return &e, nil
}

// GetExpiredEvents returns the events that have come already.
func (repo *DBEventRepository) GetExpiredEvents(ctx context.Context) ([]*Event, error) {
	rows, err := repo.db.QueryxContext(
		ctx,
		"select id, title, start_date, end_date from event where start_date < now() and notified_at is null",
	)
	if err != nil {
		return nil, err
	}

	expiredEvents := make([]*Event, 0)
	for rows.Next() {
		e := Event{}
		err := rows.StructScan(&e)
		if err != nil {
			return nil, err
		}

		expiredEvents = append(expiredEvents, &e)
	}

	return expiredEvents, nil
}

// Create creates a new event, stores it in the DB and returns the created entity object with filled ID.
func (repo *DBEventRepository) Create(ctx context.Context, e *Event) (*Event, error) {
	args := []interface{}{
		e.Title,
		e.StartDate,
		e.EndDate,
	}

	var id int64
	sql := "insert into event (title, start_date, end_date) values ($1, $2, $3) returning id"
	err := repo.db.QueryRowxContext(ctx, sql, args...).Scan(&id)
	if err != nil {
		return nil, err
	}

	e.Id = id

	return e, nil
}

// Edit replaces an event with the ID in the DB by the e event. In case of failure, it returns an error.
func (repo *DBEventRepository) Edit(ctx context.Context, e *Event) error {
	argsMap := map[string]interface{}{
		"title": e.Title,
	}
	argsMap["start_date"] = e.StartDate
	argsMap["end_date"] = e.EndDate
	argsMap["id"] = e.Id

	_, err := repo.db.NamedExecContext(
		ctx,
		"update event set title = :title, start_date = :start_date, end_date = :end_date where id = :id",
		argsMap,
	)

	return err
}

// Delete deletes an event by the ID.
func (repo *DBEventRepository) Delete(ctx context.Context, id int64) error {
	_, err := repo.db.ExecContext(
		ctx,
		"delete from event where id = $1",
		id,
	)

	return err
}

// NewDBEventRepositoryWithOptions creates a new intance of the NewDBEventRepositoryWithOptions
// connecting to the Postgres using provided credentials.
func NewDBEventRepositoryWithOptions(db *sqlx.DB) *DBEventRepository {
	return &DBEventRepository{db: db}
}
