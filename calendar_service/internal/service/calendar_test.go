package service

import (
	"context"
	"reflect"
	"testing"
	"time"

	proto "gitlab.com/forpelevin/go-calendar/calendar_service/internal/protobuf/calendar_service"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/pkg/errors"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/repository"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/serializer"
)

var (
	todayTimestamp = &timestamp.Timestamp{
		Seconds: time.Now().UTC().Unix(),
	}
	nextMonthTimestamp = &timestamp.Timestamp{
		Seconds: time.Now().UTC().AddDate(0, 1, 0).Unix(),
	}
)

func fillByExistedEvents(storage repository.EventRepository, events []*proto.Event) error {
	if events == nil {
		return nil
	}

	for _, e := range events {
		if e == nil {
			continue
		}

		_, err := storage.Create(context.Background(), serializer.SerializePbToModel(e))
		if err != nil {
			return errors.Errorf("failed to save the existed event: %+v. Reason: %v\n", e, err)
		}
	}

	return nil
}

func TestServer_CreateEvent(t *testing.T) {
	tests := []struct {
		name    string
		r       *proto.CreateEventRequest
		want    *proto.CreateEventResponse
		wantErr bool
	}{
		{
			name:    "empty request causes an error",
			r:       &proto.CreateEventRequest{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "positive case",
			r: &proto.CreateEventRequest{
				Event: &proto.Event{
					Title:     "test",
					StartDate: todayTimestamp,
					EndDate:   nextMonthTimestamp,
				},
			},
			want: &proto.CreateEventResponse{
				Event: &proto.Event{
					Id:        1,
					Title:     "test",
					StartDate: todayTimestamp,
					EndDate:   nextMonthTimestamp,
				},
			},
			wantErr: false,
		},
		{
			name: "invalid request: end date is earlier than start date",
			r: &proto.CreateEventRequest{
				Event: &proto.Event{
					Title:     "test",
					StartDate: nextMonthTimestamp,
					EndDate:   todayTimestamp,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request: start date specified but end date is not",
			r: &proto.CreateEventRequest{
				Event: &proto.Event{
					Title:     "test",
					StartDate: nextMonthTimestamp,
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewCalendarService(repository.NewMemoryEventRepository())
			got, err := s.CreateEvent(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("Server.CreateEvent() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Server.CreateEvent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestServer_UpdateEvent(t *testing.T) {
	tests := []struct {
		name          string
		existedEvents []*proto.Event
		r             *proto.UpdateEventRequest
		want          *proto.UpdateEventResponse
		wantErr       bool
	}{
		{
			name:    "invalid request without event",
			r:       &proto.UpdateEventRequest{},
			wantErr: true,
		},
		{
			name: "invalid request with end date earlier than start date",
			r: &proto.UpdateEventRequest{
				Event: &proto.Event{
					Title:     "test",
					StartDate: nextMonthTimestamp,
					EndDate:   todayTimestamp,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid request: start date specified but end date is not",
			r: &proto.UpdateEventRequest{
				Event: &proto.Event{
					Title:     "test",
					StartDate: nextMonthTimestamp,
				},
			},
			wantErr: true,
		},
		{
			name: "positive case. Update an existed event",
			existedEvents: []*proto.Event{
				{
					Id:        1,
					Title:     "name",
					StartDate: nil,
					EndDate:   nil,
				},
			},
			r: &proto.UpdateEventRequest{
				Event: &proto.Event{
					Id:        1,
					Title:     "new name",
					StartDate: todayTimestamp,
					EndDate:   nextMonthTimestamp,
				},
			},
			want: &proto.UpdateEventResponse{
				Event: &proto.Event{
					Id:        1,
					Title:     "new name",
					StartDate: todayTimestamp,
					EndDate:   nextMonthTimestamp,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storage := repository.NewMemoryEventRepository()
			// Fill the storage with existed events if they are specified.
			err := fillByExistedEvents(storage, tt.existedEvents)
			if err != nil {
				t.Error(err)
				return
			}
			s := NewCalendarService(storage)
			got, err := s.UpdateEvent(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("Server.UpdateEvent() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Server.UpdateEvent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestServer_DeleteEvent(t *testing.T) {
	tests := []struct {
		name          string
		existedEvents []*proto.Event
		r             *proto.DeleteEventRequest
		want          *proto.DeleteEventResponse
		wantErr       bool
	}{
		{
			name:    "invalid request without event ID",
			r:       &proto.DeleteEventRequest{},
			wantErr: true,
		},
		{
			name:    "invalid request: try to delete a non-existed event",
			r:       &proto.DeleteEventRequest{EventId: 1},
			wantErr: true,
		},
		{
			name: "positive case: try to delete an existed event",
			existedEvents: []*proto.Event{
				{
					Id:    1,
					Title: "test",
				},
			},
			r:       &proto.DeleteEventRequest{EventId: 1},
			want:    &proto.DeleteEventResponse{EventId: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			storage := repository.NewMemoryEventRepository()
			// Fill the storage with existed events if they are specified.
			err := fillByExistedEvents(storage, tt.existedEvents)
			if err != nil {
				t.Error(err)
				return
			}
			s := NewCalendarService(storage)
			got, err := s.DeleteEvent(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("Server.DeleteEvent() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Server.DeleteEvent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCalendarService_GetExpiredEvents(t *testing.T) {
	nextMonth := time.Now().UTC().AddDate(0, 1, 0)
	yesterday := time.Now().UTC().AddDate(0, 0, -1)
	tests := []struct {
		name            string
		eventRepository repository.EventRepository
		want            *proto.GetExpiredEventsResponse
		wantErr         bool
	}{
		{
			name: "there are no expired events",
			eventRepository: repository.NewMemoryEventRepositoryWithEvents(map[int64]*repository.Event{
				1: {
					Id: 1,
				},
				2: {
					Id:        2,
					StartDate: &nextMonth,
				},
			}),
			want: &proto.GetExpiredEventsResponse{
				Events: []*proto.Event{},
			},
		},
		{
			name: "there is an expired event",
			eventRepository: repository.NewMemoryEventRepositoryWithEvents(map[int64]*repository.Event{
				1: {
					Id: 1,
				},
				2: {
					Id:        2,
					StartDate: &yesterday,
				},
				3: {
					Id:        3,
					StartDate: &nextMonth,
				},
			}),
			want: &proto.GetExpiredEventsResponse{
				Events: []*proto.Event{
					{
						Id:        2,
						StartDate: &timestamp.Timestamp{Seconds: yesterday.Unix(), Nanos: 0},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &CalendarService{
				eventRepository: tt.eventRepository,
			}
			got, err := s.GetExpiredEvents(context.Background(), &proto.GetExpiredEventsRequest{})
			if (err != nil) != tt.wantErr {
				t.Errorf("CalendarService.GetExpiredEvents() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CalendarService.GetExpiredEvents() = %+v, want %+v", got, tt.want)
			}
		})
	}
}
