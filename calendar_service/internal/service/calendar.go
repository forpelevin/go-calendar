package service

import (
	"context"

	proto "gitlab.com/forpelevin/go-calendar/calendar_service/internal/protobuf/calendar_service"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/serializer"

	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/repository"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/validator"
)

// The calendar service.
type CalendarService struct {
	eventRepository repository.EventRepository
}

// GetEvent returns an event from the repository by passed ID.
func (s *CalendarService) GetEvent(ctx context.Context, r *proto.GetEventRequest) (*proto.GetEventResponse, error) {
	e, err := s.eventRepository.GetByID(ctx, r.EventId)
	// handle the not found error
	if err != nil && err == repository.ErrNotFound {
		return nil, status.Errorf(
			codes.NotFound,
			err.Error(),
		)
	}
	// handle other errors
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &proto.GetEventResponse{
		Event: serializer.SerializeModelToPb(e),
	}, nil
}

// GetExpiredEvents returns the expired events from a repository.
func (s *CalendarService) GetExpiredEvents(ctx context.Context, r *proto.GetExpiredEventsRequest) (*proto.GetExpiredEventsResponse, error) {
	events, err := s.eventRepository.GetExpiredEvents(ctx)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &proto.GetExpiredEventsResponse{
		Events: serializer.SerializeModelsToPb(events),
	}, nil
}

// CreateEvent creates a new event in the repository and returns a created one with ID.
func (s *CalendarService) CreateEvent(ctx context.Context, r *proto.CreateEventRequest) (*proto.CreateEventResponse, error) {
	e := serializer.SerializePbToModel(r.Event)
	err := validator.ValidateEventUpdate(e)
	if err != nil {
		return nil, err
	}

	e, err = s.eventRepository.Create(ctx, e)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &proto.CreateEventResponse{Event: serializer.SerializeModelToPb(e)}, nil
}

// UpdateEvent updates the event and returns an updated one.
func (s *CalendarService) UpdateEvent(ctx context.Context, r *proto.UpdateEventRequest) (*proto.UpdateEventResponse, error) {
	e := serializer.SerializePbToModel(r.Event)
	err := validator.ValidateEventUpdate(e)
	if err != nil {
		return nil, err
	}

	err = s.eventRepository.Edit(ctx, e)
	// handle the not found error
	if err != nil && err == repository.ErrNotFound {
		return nil, status.Errorf(
			codes.NotFound,
			err.Error(),
		)
	}
	// handle other errors
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &proto.UpdateEventResponse{Event: r.Event}, nil
}

// DeleteEvent deletes the event and returns its ID.
func (s *CalendarService) DeleteEvent(ctx context.Context, r *proto.DeleteEventRequest) (*proto.DeleteEventResponse, error) {
	err := s.eventRepository.Delete(ctx, r.EventId)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &proto.DeleteEventResponse{EventId: r.EventId}, nil
}

// NewCalendarService creates a new calendar service instance.
func NewCalendarService(eventRepository repository.EventRepository) *CalendarService {
	return &CalendarService{eventRepository: eventRepository}
}
