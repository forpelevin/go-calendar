package testutil

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/database"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/server"
)

var dbInitialized = false

// PrepareDBForTest
func PrepareDBForTest() (*sqlx.DB, error) {
	err := changeDirToRoot()
	if err != nil {
		return nil, err
	}

	// Read the env file.
	err = godotenv.Load(".env.test")
	if err != nil {
		return nil, err
	}

	c, err := server.ReadServerConfig()
	if err != nil {
		return nil, err
	}

	if !dbInitialized {
		err = initDb(c)
		if err != nil {
			return nil, err
		}
		dbInitialized = true
	}

	db, err := database.GetSqlxConnectionByConfig(c)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func initDb(conf *server.Config) error {
	// Run migrations
	sqlDb, err := database.GetSqlConnectionWithoutDatabaseByConfig(conf)
	if err != nil {
		return err
	}

	// Recreate DB
	_, err = sqlDb.Exec(fmt.Sprintf("drop database if exists %s", conf.DB.Name))
	if err != nil {
		return err
	}

	_, err = sqlDb.Exec(fmt.Sprintf("create database %s", conf.DB.Name))
	if err != nil {
		return err
	}

	return database.Migrate(conf)
}

func changeDirToRoot() error {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		return errors.New("failed to change dir to the root")
	}
	currentPath := filepath.Dir(currentFile)
	err := os.Chdir(filepath.Join(currentPath, "..", ".."))
	if err != nil {
		return err
	}

	return nil
}
