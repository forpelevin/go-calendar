package database

import (
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/server"
)

// Migrate run the stepsCount database migrations over passed DB instance. Steps count is a number that shows
// how many migrations we should to run. If it's > 0 then we migrate up, other case - down.
func Migrate(c *server.Config) error {
	m, err := migrate.New(
		"file://db/migrations",
		fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s?sslmode=disable",
			c.DB.Username,
			c.DB.Password,
			c.DB.Host,
			c.DB.Port,
			c.DB.Name,
		),
	)
	if err != nil {
		return err
	}

	err = m.Up()
	// Ignore the "no change" error because it's not valuable information.
	if err != nil && err == migrate.ErrNoChange {
		return nil
	}

	return err
}
