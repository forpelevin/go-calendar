package database

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/server"
)

// GetSqlxConnectionByConfig creates a sqlx.DB instance by the server config and connects to a DB.
// It returns an error in case of connection failure.
func GetSqlxConnectionByConfig(c *server.Config) (*sqlx.DB, error) {
	dsn := fmt.Sprintf(
		"user=%s dbname=%s password=%s sslmode=disable host=%s port=%s",
		c.DB.Username,
		c.DB.Name,
		c.DB.Password,
		c.DB.Host,
		c.DB.Port,
	)
	db, err := sqlx.Connect("pgx", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(c.DB.MaxOpenConns)
	db.SetMaxIdleConns(c.DB.MaxIdleConns)
	db.SetConnMaxLifetime(time.Duration(c.DB.MaxLifetimeSeconds) * time.Second)

	return db, nil
}

// GetSqlConnectionWithoutDatabaseByConfig creates a sql.DB instance by the server config without specifying
// a certain DB. It returns an error in case of connection failure.
func GetSqlConnectionWithoutDatabaseByConfig(c *server.Config) (*sql.DB, error) {
	dsn := fmt.Sprintf(
		"user=%s dbname=template1 password=%s sslmode=disable host=%s port=%s",
		c.DB.Username,
		c.DB.Password,
		c.DB.Host,
		c.DB.Port,
	)
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(c.DB.MaxOpenConns)
	db.SetMaxIdleConns(c.DB.MaxIdleConns)
	db.SetConnMaxLifetime(time.Duration(c.DB.MaxLifetimeSeconds) * time.Second)

	return db, nil
}
