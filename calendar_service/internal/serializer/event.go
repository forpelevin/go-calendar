package serializer

import (
	"time"

	proto "gitlab.com/forpelevin/go-calendar/calendar_service/internal/protobuf/calendar_service"

	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/repository"
)

// SerializePbInModel serializes an event in Protobuf format to the Model format.
func SerializePbToModel(pbEvent *proto.Event) *repository.Event {
	if pbEvent == nil {
		return nil
	}

	return &repository.Event{
		Id:        pbEvent.Id,
		Title:     pbEvent.Title,
		StartDate: pbTimestampToTime(pbEvent.StartDate),
		EndDate:   pbTimestampToTime(pbEvent.EndDate),
	}
}

// SerializeModelToPb serializes an event model in the Protobuf format.
func SerializeModelToPb(model *repository.Event) *proto.Event {
	if model == nil {
		return nil
	}

	return &proto.Event{
		Id:        model.Id,
		Title:     model.Title,
		StartDate: timeToPbTimestamp(model.StartDate),
		EndDate:   timeToPbTimestamp(model.EndDate),
	}
}

// SerializeModelToPb serializes the events slice to a slice of events in Protobuf format.
func SerializeModelsToPb(models []*repository.Event) []*proto.Event {
	serialized := make([]*proto.Event, 0, len(models))
	for _, e := range models {
		serialized = append(serialized, SerializeModelToPb(e))
	}

	return serialized
}

func pbTimestampToTime(pb *timestamp.Timestamp) *time.Time {
	if pb != nil {
		t := time.Unix(pb.Seconds, 0)
		return &t
	}

	return nil
}

func timeToPbTimestamp(t *time.Time) *timestamp.Timestamp {
	if t == nil {
		return nil
	}

	return &timestamp.Timestamp{Seconds: t.Unix(), Nanos: 0}
}
