package validator

import (
	"github.com/pkg/errors"
	"gitlab.com/forpelevin/go-calendar/calendar_service/internal/repository"
)

var (
	// Errors
	errNullEvent = errors.New("event entity is required")
	errDate      = errors.New("end date cannot be earlier than start date")
)

// ValidateEventUpdate validates that given event is valid to update and returns an error if it's not.
func ValidateEventUpdate(event *repository.Event) error {
	validators := []func(event *repository.Event) error{checkIsNotNil, checkDates}
	for _, validateFunc := range validators {
		err := validateFunc(event)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkIsNotNil(event *repository.Event) error {
	if event == nil {
		return errNullEvent
	}

	return nil
}

func checkDates(event *repository.Event) error {
	if event.StartDate == nil && event.EndDate == nil {
		return nil
	}

	if event.StartDate != nil && event.EndDate == nil {
		return errDate
	}

	if event.StartDate.UTC().Unix() > event.EndDate.UTC().Unix() {
		return errDate
	}

	return nil
}
