# Go Calendar Service
The microservice implements a wrapper for a Calendar.

## Getting Started
```
git clone https://gitlab.com/forpelevin/go-calendar.git
```
## Prerequisites
For the successful using you should have:
```
go >= 1.12
docker CE 18+
```
## Build the project
The command starts up the docker-compose with PostgreSQL instance. 
It uses configuration from the .env.test file. Also, there will be run all necessary migrations in the DB.
```
make build
```
## Running the tests
It's a good practice to run the tests before using the service to make sure everything is OK.
```
cd $GOPATH/src/gitlab.com/forpelevin/go-calendar 
make test
```
## Build the app and copy ENV file if it's not been copied yet
```
cd $GOPATH/src/gitlab.com/forpelevin/go-calendar
make build
cp -n .env.dist .env
```
## Sample of using
```
./main
```
Now you can use the service via gRPC using the 8080 port to connect.
## License
This project is licensed under the MIT License.
