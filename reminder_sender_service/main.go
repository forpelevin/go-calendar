package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	proto2 "github.com/golang/protobuf/proto"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"github.com/segmentio/kafka-go"
	proto "gitlab.com/forpelevin/go-calendar/reminder_sender_service/internal/protobuf/calendar_service"
)

const (
	DbMAxOpenConns                 = 5
	DbMaxIdleConnections           = 100
	DbConnectionMaxLifetimeSeconds = 300 * time.Second
)

var (
	notificationSent = promauto.NewCounter(prometheus.CounterOpts{
		Name: "reminderservice_notifications_sent_total",
		Help: "The total number of sent notifications",
	})
)

// Notifier a service for sending notifications about events.
type Notifier interface {
	// Notify sends a notification about given event.
	Notify(ctx context.Context, e *proto.Event) error
}

// DbNotifier persists info about notification in DB.
type DbNotifier struct {
	db *sqlx.DB
}

// Notify updates the event information that it's been notified.
func (notifier *DbNotifier) Notify(ctx context.Context, e *proto.Event) error {
	_, err := notifier.db.ExecContext(
		ctx,
		"update event set notified_at = now() where id = $1",
		e.Id,
	)

	return err
}

func initPrometheusHandler() {
	http.Handle("/metrics", promhttp.Handler())

	// Start the HTTP server for Prometheus metrics pulling.
	promLisAddr := fmt.Sprintf(
		"%s:%s",
		os.Getenv("PROMETHEUS_LISTEN_HOST"),
		os.Getenv("PROMETHEUS_LISTEN_PORT"),
	)
	go func() {
		fmt.Printf("Listening on: %s for Prometheus\n", promLisAddr)
		log.Fatal(http.ListenAndServe(promLisAddr, nil))
	}()
}

func main() {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Listen to a queue
	// Read the env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	initPrometheusHandler()

	kafkaReader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{fmt.Sprintf("%s:%s", os.Getenv("KAFKA_HOST"), os.Getenv("KAFKA_PORT"))},
		Topic:   os.Getenv("KAFKA_EXPIRED_EVENTS_TOPIC"),
	})
	defer kafkaReader.Close()

	dbNotifier := NewDbNotifier()
	ctx := context.Background()

	// Handle the expired events.
	for {
		// Read a new massage from the Kafka's topic of expired events.
		m, err := kafkaReader.ReadMessage(ctx)
		if err != nil {
			log.Fatal(err)
		}

		// Ignore the empty messages.
		if len(m.Value) == 0 {
			continue
		}

		// Decode the message value using protobuf
		var e proto.Event
		err = proto2.Unmarshal(m.Value, &e)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("read the event: %+v\n", e)

		// Notify owner about the event.
		err = dbNotifier.Notify(ctx, &e)
		if err != nil {
			log.Fatal(err)
		}
		notificationSent.Inc()
	}
}

// NewDbNotifier creates a new instance of the DBNotifier using ENV params.
func NewDbNotifier() *DbNotifier {
	dsn := fmt.Sprintf(
		"user=%s dbname=%s password=%s sslmode=disable host=%s port=%s",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
	)
	db, err := sqlx.Connect("pgx", dsn)
	if err != nil {
		log.Fatal(err)
	}
	db.SetMaxOpenConns(DbMAxOpenConns)
	db.SetMaxIdleConns(DbMaxIdleConnections)
	db.SetConnMaxLifetime(DbConnectionMaxLifetimeSeconds)

	return &DbNotifier{
		db: db,
	}
}
