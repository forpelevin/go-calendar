module gitlab.com/forpelevin/go-calendar/reminder_sender_service

go 1.12

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/golang/protobuf v1.3.2
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.2.1
	github.com/segmentio/kafka-go v0.3.3
	github.com/shopspring/decimal v0.0.0-20191009025716-f1972eb1d1f5 // indirect
	google.golang.org/grpc v1.24.0
)
