-include .env
export

install:
	# Generate the protobuf
	protoc --go_out=plugins=grpc:calendar_service/internal protobuf/calendar_service/calendar.proto
	protoc --go_out=plugins=grpc:reminder_cron_service/internal protobuf/calendar_service/calendar.proto
	protoc --go_out=plugins=grpc:reminder_sender_service/internal protobuf/calendar_service/calendar.proto
	protoc --go_out=plugins=grpc:integration_tests/internal protobuf/calendar_service/calendar.proto

	# Build the services with envs.
	cp -n .env.dist .env || \
	cd integration_tests && make build
	cd calendar_service && make build
	cd reminder_cron_service && make build
	cd reminder_sender_service && make build

up:
	docker-compose -f docker-compose.yml up -d --build calendar_grpc reminder_sender reminder_cron

test:
	docker-compose -f docker-compose.test.yml up --abort-on-container-exit --build
	docker-compose -f docker-compose.test.yml down

evans:
	evans --host=localhost --port=${GRPC_SERVER_PORT} --package=proto ./protobuf/calendar_service/calendar.proto