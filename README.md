# Go Calendar Application
The app consists of several microservices. There are:
1) Calendar service. gRPC service that implements CRUD operations under the Event entity.
2) Reminder cron service. The service endlessly scan for expired events (where start date is in past), and then sends the events to the Kafka expired events Topic.
3) Reminder sender service. The service listens for events from the Kafka expired events topic and then sends notifications about the event. For now, there just a mock that sets the notified_at field in an expired event and saves it in DB. But the logic is dependent from an abstraction so feel free to implement your own notificator and use it.

Also there are extra subdirs. The first one is the "protobuf" - the place where all proto files are stored. The second one contains the integration tests for the app.

The application has a configured monitoring using the Prometheus. You can see the metrics on localhost:9090.
## Getting Started
```
git clone https://gitlab.com/forpelevin/go-calendar.git
```
## Dependencies
You need to have only these services:
```
docker
docker-compose
```
## Installation
Firstly, run the command to generate necessary protobuf files and copy .env.dist into .env in each microservice.
```
make install
```
## Integration testing
Run the command to make sure that everything is OK.
```
make test
```
## Start up the app
The command will up all microservices using docker-compose.
```
make up
```
## License
This project is licensed under the MIT License.
