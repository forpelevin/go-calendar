module gitlab.com/forpelevin/go-calendar/reminder_cron_service

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/joho/godotenv v1.3.0
	github.com/segmentio/kafka-go v0.3.3
	google.golang.org/grpc v1.24.0
)
