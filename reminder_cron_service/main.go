package main

import (
	"context"
	"fmt"
	proto2 "github.com/golang/protobuf/proto"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"
	"github.com/segmentio/kafka-go"
	proto "gitlab.com/forpelevin/go-calendar/reminder_cron_service/internal/protobuf/calendar_service"
	"google.golang.org/grpc"
)

func main() {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Read the env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	kafkaAddress := fmt.Sprintf("%s:%s", os.Getenv("KAFKA_HOST"), os.Getenv("KAFKA_PORT"))
	fmt.Printf("Kafka address: %s\n", kafkaAddress)
	kafkaWriter := kafka.NewWriter(kafka.WriterConfig{
		Brokers:  []string{kafkaAddress},
		Topic:    os.Getenv("KAFKA_EXPIRED_EVENTS_TOPIC"),
		Balancer: &kafka.RoundRobin{},
	})
	defer kafkaWriter.Close()

	// Connect to the gRPC server via TCP.
	address := fmt.Sprintf(
		"%s:%s",
		os.Getenv("CALENDAR_GRPC_SERVER_HOST"),
		os.Getenv("CALENDAR_GRPC_SERVER_PORT"),
	)
	fmt.Printf("address: %s\n", address)
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		log.Fatal(err)
	}
	calendarServiceClient := proto.NewCalendarServiceClient(conn)
	ctx := context.Background()

	for {
		// Get expired events from DB
		res, err := calendarServiceClient.GetExpiredEvents(ctx, &proto.GetExpiredEventsRequest{})
		fmt.Printf("Expired events: %+v\n", res)
		if err != nil {
			log.Fatal(err)
		}

		// Convert events to messages
		messages := make([]kafka.Message, 0, len(res.Events))
		for _, e := range res.Events {
			// Create a message key like the event ID in bytes format.
			key := []byte(strconv.Itoa(int(e.Id)))
			// Encode the message body using protobuf
			value, err := proto2.Marshal(e)
			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("Kafka key: %+v\n and kafka value: %+v\n", key, value)
			messages = append(messages, kafka.Message{
				Key:   key,
				Value: value,
			})
		}

		// If there are messages then put them in Kafka..
		if len(messages) > 0 {
			fmt.Printf("Writing messages... %+v\n", messages)
			err = kafkaWriter.WriteMessages(ctx, messages...)
			if err != nil {
				log.Fatal(err)
			}
		}

		fmt.Printf("Going to sleep...\n")
		time.Sleep(3 * time.Second)
	}
}
